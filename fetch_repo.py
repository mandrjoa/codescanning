import os
import git
import random

REPOSITORIES = [
    "https://github.com/psf/requests.git",
    "https://github.com/django/django.git",
    "https://github.com/pallets/flask.git"
]

def fetch_random_repo():
    repo_url = random.choice(REPOSITORIES)
    repo_name = repo_url.split('/')[-1].replace('.git', '')
    repo_path = os.path.join("random_repo", repo_name)
    
    if not os.path.exists(repo_path):
        os.makedirs(repo_path)
    
    try:
        git.Repo.clone_from(repo_url, repo_path)
        print(f"Successfully cloned repository: {repo_url}")
    except git.exc.GitCommandError as e:
        print(f"Error: {e}")

if __name__ == "__main__":
    fetch_random_repo()
